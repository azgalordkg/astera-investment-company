from django.urls import path
from .views import BaseView, IndexView

urlpatterns = [
    path('base', BaseView.as_view()),
    path('', IndexView.as_view())
]