from django.contrib import admin

# Register your models here.
from main.models import *


class SocialInline(admin.TabularInline):
    model = Social


class ContactsAdmin(admin.ModelAdmin):
    inlines = (SocialInline,)


class TrendInline(admin.TabularInline):
    model = Trend


class AboutUsAdmin(admin.ModelAdmin):
    inlines = (TrendInline,)


admin.site.register(Contacts, ContactsAdmin)
admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(Slogan)
admin.site.register(Stages)
admin.site.register(People)
admin.site.register(Document)
