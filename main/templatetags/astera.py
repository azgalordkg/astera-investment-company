import pypugjs
from django import template

register = template.Library()

@pypugjs.register_filter('is_even')
@register.filter(name='is_even')
def is_even(v):
  return int(v) % 2 == 0

