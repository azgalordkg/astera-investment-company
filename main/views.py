import requests
from django.shortcuts import render
from django.views.generic import TemplateView

from main.models import *


class BaseView(TemplateView):
    template_name = 'base.pug'


class IndexView(TemplateView):
    template_name = 'index.pug'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['contacts'] = Contacts.objects.first()
        context['social'] = Social.objects.first()
        context['about_us'] = AboutUs.objects.first()
        context['trend'] = Trend.objects.all()
        context['slogan'] = Slogan.objects.filter(active=True)
        context['stages'] = Stages.objects.all()
        context['people'] = People.objects.all()
        context['document'] = Document.objects.order_by('-created_at').first()
        return context

