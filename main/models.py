from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.


class User(AbstractUser):
    pass


class Contacts(models.Model):
    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'

    email = models.EmailField(verbose_name='Почта')
    phone = models.CharField(verbose_name='Номер телефона', max_length=255)

    def __str__(self):
        return self.email


class Social(models.Model):
    class Meta:
        verbose_name = 'Социальная сеть'
        verbose_name_plural = 'Социальные сети'

    link_fb = models.URLField(verbose_name='Ссылка на фейсбук', null=True)
    link_insta = models.URLField(verbose_name='Ссылка на инстаграм', null=True)
    link_youtube = models.URLField(verbose_name='Ссылка на ютуб', null=True)
    contacts = models.ForeignKey(Contacts, verbose_name='Контакты', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "Соц.сети"


class AboutUs(models.Model):
    class Meta:
        verbose_name = 'О нас'
        verbose_name_plural = 'О нас'

    text = RichTextUploadingField(verbose_name='Текст')

    def __str__(self):
        return "Информация о нас"


class Trend(models.Model):
    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'

    image = models.FileField(verbose_name='Картинка')
    title = models.CharField(verbose_name='Заголовок', max_length=50)
    text = models.TextField(verbose_name='Описание', max_length=200)
    about_us = models.ForeignKey(AboutUs, verbose_name='О нас', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title


class Slogan(models.Model):
    class Meta:
        verbose_name = 'Слоган'
        verbose_name_plural = 'Слоганы'

    text = models.TextField(verbose_name='Текст')
    author = models.CharField(verbose_name='Автор', max_length=255)
    active = models.BooleanField(verbose_name='Показать на главной', default=True,
                                 help_text='Если хотите чтобы этот слоган не показывался на странице, уберите галочку')

    def __str__(self):
        return self.author


class Stages(models.Model):
    class Meta:
        verbose_name = 'Этап'
        verbose_name_plural = 'Этапы'

    title = models.CharField(verbose_name='Название этапа', max_length=255)
    description = RichTextUploadingField(verbose_name='Описание')

    def __str__(self):
        return self.title


class People(models.Model):
    class Meta:
        verbose_name = 'Личность'
        verbose_name_plural = 'Люди'

    photo = models.ImageField(verbose_name='Фото')
    description = RichTextUploadingField(verbose_name='Описание')

    def __str__(self):
        return 'People'


class Document(models.Model):
    class Meta:
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'

    document = models.FileField(verbose_name='Документ для скачивания')
    created_at = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __str__(self):
        return 'Документ'
